# Primer 1

Napisati klasu radniku. Radnik je opisan imenom, prezimenom i visinom plate. Napisati klasu PlatnaLista. Ova klasa sadrži kolekciju radnika. Platnu listu je moguće pretraživati po proizvoljnim kriterijumima. Rezultat pretrage treba da je nova platna lista koja u sebi sadrži samo one zaposlene koji ispunjavaju kriterijum pretrage.