# Primer 4

Proširiti prethodno rešenje uvođenjem mogućnosti za sortiranje kolekcije. Sortiranje treba realizovati kroz metodu koja podržava upotrebu proizvoljno definisanog komparatora. Uopštiti definicije funkcionalnih interfejsa za filtriranje, mapiranje i redukciju.