#include <iostream>
#include "headers/Vektor.hpp"

using namespace std;

int main() {
	Vektor *v1 = new Vektor(5);
	Vektor *v2 = new Vektor(5);
	for(int i = 0; i < 5; i++) {
		v1->add(i*10);
		v2->add(i);
	}

	for(int i = 0; i < v1->length(); i++) {
		cout << (*v1)[i] << endl;
		cout << (*v2)[i] << endl;
	}

	Vektor *v3 = (*v1)+(*v2);
	for(int i = 0; i < v1->length(); i++) {
		cout << (*v1)[i] << endl;
		cout << (*v2)[i] << endl;
		cout << (*v3)[i] << endl;
	}
}
