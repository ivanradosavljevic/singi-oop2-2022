#ifndef HEADERS_VEKTOR_HPP_
#define HEADERS_VEKTOR_HPP_

class Vektor {
private:
	int capacity;
	int size;
	double *elements;
public:
	Vektor();
	Vektor(int capacity);
	void add(double value);
	void remove(int indeks);
	int length() {return this->size;};
	double operator[](int indeks);
	Vektor* operator+(Vektor &other);
	Vektor* operator+(Vektor *other);
	Vektor* operator-(Vektor &other);
	Vektor* operator-(Vektor *other);
	bool operator==(Vektor &other);
	bool operator==(Vektor *other);
	~Vektor();
};



#endif /* HEADERS_VEKTOR_HPP_ */
