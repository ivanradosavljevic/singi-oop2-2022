#include "../headers/Vektor.hpp"

Vektor::Vektor(int capacity) :capacity(capacity), size(0), elements(new double[capacity]) {

}

Vektor::Vektor() : capacity(10), size(0), elements(new double[capacity]) {

}

void Vektor::add(double value) {
	this->elements[this->size] = value;
	this->size++;
}

void Vektor::remove(int indeks) {
	for(int i = indeks; i < this->size-1; i++) {
		this->elements[i] = this->elements[i+1];
	}
	this->size--;
}

double Vektor::operator [](int indeks) {
	return this->elements[indeks];
}

Vektor* Vektor::operator +(Vektor &other) {
	Vektor *v = new Vektor(this->capacity);
	for(int i = 0; i < this->length(); i++) {
		v->elements[i] = (*this)[i] + other[i];
	}
	return v;
}

Vektor* Vektor::operator -(Vektor &other) {
	Vektor *v = new Vektor(this->capacity);
	for(int i = 0; i < this->length(); i++) {
		v->elements[i] = (*this)[i] - other[i];
	}
	return v;
}

bool Vektor::operator ==(Vektor &other) {
	for(int i = 0; i < this->length(); i++) {
		if((*this)[i] != other[i]) {
			return false;
		}
	}
	return true;
}

Vektor::~Vektor() {
	delete [] elements;
}
