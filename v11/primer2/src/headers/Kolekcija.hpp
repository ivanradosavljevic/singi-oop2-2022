#ifndef HEADERS_KOLEKCIJA_HPP_
#define HEADERS_KOLEKCIJA_HPP_


template<typename T>
class Kolekcija {
private:
	int capacity;
	int size;
	T *elements;
public:
	Kolekcija();
	Kolekcija(int capacity);
	void add(T value);
	void remove(int indeks);
	int length() {return this->size;};
	T operator[](int indeks);
	~Kolekcija();
};

template<typename T>
Kolekcija<T>::Kolekcija(int capacity) :capacity(capacity), size(0), elements(new T[capacity]) {

}
template<typename T>
Kolekcija<T>::Kolekcija() : capacity(10), size(0), elements(new T[capacity]) {

}

template<typename T>
void Kolekcija<T>::add(T value) {
	this->elements[this->size] = value;
	this->size++;
}

template<typename T>
void Kolekcija<T>::remove(int indeks) {
	for(int i = indeks; i < this->size-1; i++) {
		this->elements[i] = this->elements[i+1];
	}
	this->size--;
}

template<typename T>
T Kolekcija<T>::operator [](int indeks) {
	return this->elements[indeks];
}


#endif /* HEADERS_KOLEKCIJA_HPP_ */
