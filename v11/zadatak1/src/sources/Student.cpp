#include "../headers/Student.hpp"

Student::Student() : Osoba() {

}

Student::Student(string ime, string prezime, string jmbg, string brojIndeksa) : Osoba(ime, prezime, jmbg), brojIndeksa(brojIndeksa) {

}

void Student::dodajPredmet(Predmet *predmet, bool rekurzivno) {
	pohadjanja.push_back(predmet);
	if(rekurzivno) {
		predmet->dodajStudenta(this, false);
	}
}

void Student::ukloniPredmet(Predmet *predmet, bool rekurzivno) {
	bool found = false;
	size_t i = 0;
	for(; i < pohadjanja.size(); i++) {
		if(pohadjanja[i] == predmet) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	if(rekurzivno) {
		pohadjanja[i]->ukloniStudenta(this, false);
	}

	pohadjanja.erase(pohadjanja.begin()+i);
}

void Student::detalji() {
	cout << ime << " " << prezime << " " << jmbg << " " << brojIndeksa;
}

Student::~Student() {

}
