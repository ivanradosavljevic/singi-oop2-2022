#include "../headers/RegistarOcena.hpp"

RegistarOcena::RegistarOcena() {

}

void RegistarOcena::dodajOcenu(ProveraZnanja *provera) {
	provere.push_back(provera);
}

void RegistarOcena::ukloniOcenu(ProveraZnanja *provera) {
	bool found = false;
	size_t i = 0;
	for(; i < provere.size(); i++) {
		if(provere[i] == provera) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	provere.erase(provere.begin()+i);
}

IzvodOcena* RegistarOcena::operator [](string indeks) {
	IzvodOcena* izvod = new IzvodOcena();
	for(size_t i = 0; i < provere.size(); i++) {
		if(provere[i]->sifraPredmeta() == indeks) {
			izvod->push_back(provere[i]);
		}
	}
	return izvod;
}

RegistarOcena::~RegistarOcena() {

}
