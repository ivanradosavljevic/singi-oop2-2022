package rs.ac.singidunum.novisad.primer4;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

	public static void main(String[] args) {
		List<Integer> lista = Stream.iterate(10, (v) -> v + 2).limit(10).collect(Collectors.toList());
		for(Integer i : lista) {
			System.out.println(i);
		}
		System.out.println("");
		Stream.iterate(10, (v) -> v + 2).limit(10).peek(x -> System.out.println(x)).count();
	}
}
