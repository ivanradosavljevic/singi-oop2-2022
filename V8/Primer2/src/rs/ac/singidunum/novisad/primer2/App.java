package rs.ac.singidunum.novisad.primer2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import rs.ac.singidunum.novisad.primer2.model.Radnik;

public class App {

	public static void main(String[] args) {
		ArrayList<Radnik> radnici = new ArrayList<Radnik>();
		radnici.add(new Radnik("Marko", "Markovic", 50000, "Proizvodnja"));
		radnici.add(new Radnik("Petar", "Markovic", 50000, "Proizvodnja"));
		radnici.add(new Radnik("Milica", "Petrovic", 44200, "Proizvodnja"));
		radnici.add(new Radnik("Ana", "Markovic", 57000, "Prodaja"));
		radnici.add(new Radnik("Sara", "Jovanovic", 56000, "Proizvodnja"));
		radnici.add(new Radnik("Sasa", "Pavlovic", 35900, "Prodaja"));
		radnici.add(new Radnik("Marko", "Miletic", 45300, "Proizvodnja"));
		radnici.add(new Radnik("Nikola", "Jakovljevic", 125000, "Uprava"));

		List<Radnik> filtrirani = radnici.stream().filter(x -> x.getVisinaPlate() >= 50000)
				.collect(Collectors.toList());

		for (Radnik r : filtrirani) {
			System.out.println(r);
		}

		double ukupno = filtrirani.stream().map(r -> r.getVisinaPlate()).reduce((acc, v) -> acc + v).get();
		System.out.println(ukupno);
	}

}
