# Zadatak 2
1. Napraviti klase Racun i Proizvod. Racun je opisan spiskom proizvoda, dok su proizvodi opisani nazivom i cenom.
2. Napraviti tok računa i u njega dodati nekoliko računa sa proizvodima.
3. Zapisati sadržaj toka računa u datoteku gde je svaki red jedan račun, odnosno gde svaki red sadrži spisak prodatih proizvoda.
4. Preko tokova napraviti učitavanje datoteke i njenu transformaciju u niz objekata. Za sve učitane objekte ispisati njihove vrednosti.