package rs.ac.singidunum.novisad.primer3;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import rs.ac.singidunum.novisad.primer3.model.Proizvod;
import rs.ac.singidunum.novisad.primer3.model.StavkaPorudzbine;

public class App {

	public static void main(String[] args) {
		Stream<Proizvod> proizvodi = Stream.<Proizvod>builder().add(new Proizvod("Brasno", 45)).add(new Proizvod("Mleko", 90))
				.add(new Proizvod("Ulje", 190)).build();
		
		List<StavkaPorudzbine> stavke = proizvodi.map(p -> new StavkaPorudzbine(p, 3.0)).collect(Collectors.toList());
		
		for(StavkaPorudzbine s : stavke) {
			System.out.println(s);
		}
	}

}
