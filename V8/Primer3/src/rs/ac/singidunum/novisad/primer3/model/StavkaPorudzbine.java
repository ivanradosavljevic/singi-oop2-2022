package rs.ac.singidunum.novisad.primer3.model;

public class StavkaPorudzbine {
	private Proizvod proizvod;
	private double kolicina;

	public StavkaPorudzbine(Proizvod proizvod, double kolicina) {
		this.proizvod = proizvod;
		this.kolicina = kolicina;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	public double getKolicina() {
		return kolicina;
	}

	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}

	@Override
	public String toString() {
		return "StavkaPorudzbine [proizvod=" + proizvod + ", kolicina=" + kolicina + "]";
	}
}
