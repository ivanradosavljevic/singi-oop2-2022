# Primer 5

Upotrebom tokova napraviti parser CSV dokumenta koji sadrži podatke o ocenama. Svaki red u CSV dokumentu sadrži naziv predmeta, ocenu i broj espb bodova. Prvi red u CSV dokumentu je zaglavlje i ne predstavlja sadržaj koji treba parsirati. Za učitane podatke izračunati sumu ESPB bodova. Zapisati sadržaj učitanog toka u datoteku.