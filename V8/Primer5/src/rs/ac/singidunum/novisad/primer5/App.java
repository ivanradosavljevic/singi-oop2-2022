package rs.ac.singidunum.novisad.primer5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import rs.ac.singidunum.novisad.primer5.model.Ocena;

public class App {

	public static void main(String[] args) throws IOException {
		Supplier<Stream<Ocena>> oceneSupplier = () -> {
			try {
				return Files.lines(Paths.get("ocene.txt")).skip(1).map(o -> o.split(","))
						.map(o -> new Ocena(o[0], Double.parseDouble(o[1]), Double.parseDouble(o[2])));
			} catch (IOException e) {
				e.printStackTrace();
			}
			return Stream.empty();
		};
		
		oceneSupplier.get().peek(x -> System.out.println(x)).count();
		System.out.println(oceneSupplier.get().map(o -> o.getOcena()).reduce((acc, o) -> acc + o));
		Files.write(Paths.get("ocene2.txt"), oceneSupplier.get().map(String::valueOf).collect(Collectors.toList()));
	}

}
