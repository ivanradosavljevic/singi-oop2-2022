# Zadatak 1

1. Napraviti program za razmenu poruka. Svaka poruka je opisana tekstom i autorom. Autor je opisan korisničkim imenom i email adresom.
2. Korisnici mogu da šalju poruke na kanale. Svaki kanal ima svoj naziv i spisak primljenih poruka. Omogućiti ispis sadržaja kanala tako da se svaka poruka ispisuje u formatu: DATUM_I_VREME_PORUKE - KORISNIČKO_IME: SADRŽAJ_PORUKE.
3. Omogućiti korisnicima da šalju poruke jedni drugima. Svaki korisnik mora da sadrži podatke o porukama koje je primio i poslao. Poruke organizovati u privatne kanale koji služe za direktnu razmenu poruka.
4. Uvesti ograničenje da korisnici moraju biti prijavljeni u kanal u koji žele da pošalju poruke.
5. Uvesti ograničenje da privatni kanali mogu sadržati maksimalno dva korisnika.