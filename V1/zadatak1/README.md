# Zadatak 1

1. Napisati klasu TehnickiProizvod. Ova klasa sadrži atribute marka, model, kategorija i cena. Kategorija je opisana nazivom kategorije i šifrom kategorije.
2. Napisati klasu Televizor koja sadrži atribut dijagonala.
3. Napisati klasu Sporet koja sadrži atribute brojRingli, imaRernu, boja, duzina, sirina i visina.
4. Napisati klasu Osoba koja sadrži atribute ime i prezime.
5. Napisati klasu Zaposleni koja sadrži atribut šifraZaposlenog.
6. Napisati klasu Prodavac, prodavac nema nikakve dodatne atribute.
7. Napisati klasu račun koja sadrži atribute prodavac i proizvodi. Atribut proizvodi je kolekcija proizvoda dodatih na račun. Klasa račun sadrži metodu cena koja vraća ukupnu cenu svih proizvoda dodatih na račun.
8. U svakoj od navedenih klasa omogućiti ispisivanje podataka.
9. U main metodi napraviti kolekcije zaposlenih, tehničkih proizvoda i računa, ispisati sadržaj svake od kolekcija.
10. Uvesti klasu Prodavnica koja sadrži spisak dostupnih tehničkih proizvoda, spisak zaposlenih i metode za dodavanje i uklanjanje proizvoda i zaposlenih, kao i metodu za ispis svih podataka o prodavnici.
11. U klasu Prodavnica dodati metodu za kreiranje računa, račun se kreira prosleđivanjem prodavca iz prodavnice i spisak proizvoda iz prodavnice.
12. Za klase prodavnica i račun definisati metode za osnovne operacije nad kolekcijama: dodavanje, uklanjanje, pronalaženje po indeksu. Metode u obe klase treba da rade na isti način. Testirati ispravnost metoda instanciranjem prodavnice i računa i pozivanjem odgovarajućih metoda u main metodi.