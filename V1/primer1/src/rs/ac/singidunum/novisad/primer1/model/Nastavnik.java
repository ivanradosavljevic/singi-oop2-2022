package rs.ac.singidunum.novisad.primer1.model;

public class Nastavnik {
	private String ime;
	private String prezime;
	private String zvanje;
	
	public Nastavnik() {
		super();
	}
	public Nastavnik(String ime, String prezime, String zvanje) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.zvanje = zvanje;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getZvanje() {
		return zvanje;
	}
	public void setZvanje(String zvanje) {
		this.zvanje = zvanje;
	}
	
	public void prikaz() {
		System.out.println(this.ime + " " + this.prezime + ", " + this.zvanje);
	}
}
