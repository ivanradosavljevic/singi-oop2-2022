# Primer 1

1. Napisati klasu Student, ova klasa je opisana atributima ime, przime i brojIndeksa.
2. Napisati klasu Nastavnik, ova klasa je opisana atributima ime, prezime i zvanje.
3. U obe klase dodati metodu prikaz() koja ispisuje podatke studentu, odnosno nastavniku.
4. Napisati klasu App koja sadrži main metodu. U main metodi instancirati studenta i nastavnika. Pozvati metodu prikaz() nad instancama.