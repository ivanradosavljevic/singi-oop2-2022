package rs.ac.singidunum.novisad.primer2.model;

public class Osoba {
	protected String ime;
	protected String prezime;
	
	public Osoba() {
		super();
	}
	public Osoba(String ime, String prezime) {
		super();
		this.ime = ime;
		this.prezime = prezime;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public void prikaz(String krajReda) {
		System.out.print(this.ime + " " + this.prezime + krajReda);
	}
	public void prikaz() {
		this.prikaz("\n");
	}
}
