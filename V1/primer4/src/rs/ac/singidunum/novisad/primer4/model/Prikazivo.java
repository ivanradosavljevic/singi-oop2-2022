package rs.ac.singidunum.novisad.primer4.model;

public interface Prikazivo {
	public void prikaz();
	public void prikaz(String krajReda);
}
