package rs.ac.singidunum.novisad.primer4.model;

public class Nastavnik extends Radnik {
	private String zvanje;
	
	public Nastavnik() {
		super();
	}
	public Nastavnik(String ime, String prezime, String sifraZaposlenog, String zvanje) {
		super(ime, prezime, sifraZaposlenog);
		this.zvanje = zvanje;
	}
	public String getZvanje() {
		return zvanje;
	}
	public void setZvanje(String zvanje) {
		this.zvanje = zvanje;
	}
	@Override
	public void prikaz() {
		super.prikaz("");
		System.out.println(", " + this.zvanje);
	}
}
