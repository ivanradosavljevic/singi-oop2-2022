package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;
import java.util.List;

public class JavaGenerator implements Generator {
	protected String generisiListuParametara(List<Parametar> parametri) {
		String kod = "";
		if (parametri.size() > 0) {
			for (int i = 0; i < parametri.size() - 1; i++) {
				kod += this.generisi(parametri.get(i)) + ", ";
			}
			kod += this.generisi(parametri.get(parametri.size() - 1));
		}
		return kod;
	}

	protected String generisiDeklaracijuMetode(Metoda metoda) {
		String kod = metoda.getVidljivost() + " ";
		if (metoda.isStaticka()) {
			kod += "static ";
		}
		kod += metoda.getTipPovratneVrednosti() + " " + metoda.getNaziv() + "("
				+ generisiListuParametara(metoda.getParametri()) + ")";
		return kod;
	}

	@Override
	public String generisi(Parametar parametar) {
		return parametar.getTip() + " " + parametar.getNaziv();
	}

	@Override
	public String generisi(Atribut atribut) {
		return atribut.getVidljivost() + " " + (atribut.isStaticki() ? "static " : "")
				+ (atribut.isKonstanta() ? "final " : "") + atribut.getTip() + " " + atribut.getNaziv()
				+ (atribut.getPocetnaVrednost() == null ? "" : " = " + atribut.getPocetnaVrednost()) + ";";
	}

	@Override
	public String generisi(Metoda metoda) {
		return generisiDeklaracijuMetode(metoda) + " {\n}";
	}

	@Override
	public String generisi(Interfejs interfejs) {
		String kod = interfejs.getVidljivost() + " interface " + interfejs.getNaziv();
		if (interfejs.getNasledjuje().size() > 0) {
			kod += " extends ";
			for (int i = 0; i < interfejs.getNasledjuje().size() - 1; i++) {
				kod += interfejs.getNasledjuje().get(i).getNaziv() + ", ";
			}
			kod += interfejs.getNasledjuje().get(interfejs.getNasledjuje().size() - 1).getNaziv();
		}
		kod += " {\n";
		for (Atribut a : interfejs.getAtributi()) {
			kod += "\t" + generisi(a) + "\n";
		}
		for (Metoda m : interfejs.getMetode()) {
			kod += "\t" + generisiDeklaracijuMetode(m);
			if (!m.isStaticka()) {
				kod += ";\n";
			} else {
				kod += " { }\n";
			}
		}
		kod += "}\n";
		return kod;
	}

	@Override
	public String generisi(Klasa klasa) {
		String kod = klasa.getVidljivost();
		if(klasa.isApstratka()) {
			kod += " abstract";
		}
		kod += " class " + klasa.getNaziv();
		ArrayList<Klasa> klase = new ArrayList<Klasa>();
		ArrayList<Interfejs> interfejsi = new ArrayList<Interfejs>();

		for (SlozeniTip st : klasa.getNasledjuje()) {
			if (st instanceof Klasa) {
				klase.add((Klasa) st);
			} else if (st instanceof Interfejs) {
				interfejsi.add((Interfejs) st);
			}
		}
		if (klase.size() > 1) {
			throw new RuntimeException("Klasa ne sme nasledjivati vise klasa!");
		} else {
			if (klase.size() > 0) {
				kod += " extends ";
				kod += klasa.getNasledjuje().get(0).getNaziv();
			}
		}

		if (interfejsi.size() > 0) {
			kod += " implements ";
			for (int i = 0; i < interfejsi.size() - 1; i++) {
				kod += interfejsi.get(i).getNaziv() + ", ";
			}
			kod += interfejsi.get(interfejsi.size() - 1).getNaziv();
		}

		kod += " {\n";
		for (Atribut a : klasa.getAtributi()) {
			kod += "\t" + generisi(a) + "\n";
		}
		for (Metoda m : klasa.getMetode()) {
			kod += "\t" + generisiDeklaracijuMetode(m) + " { }\n";
		}
		kod += "}\n";
		return kod;
	}
}
