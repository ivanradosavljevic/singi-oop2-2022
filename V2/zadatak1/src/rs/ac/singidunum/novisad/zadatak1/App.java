package rs.ac.singidunum.novisad.zadatak1;

import rs.ac.singidunum.novisad.zadatak1.model.Atribut;
import rs.ac.singidunum.novisad.zadatak1.model.Generator;
import rs.ac.singidunum.novisad.zadatak1.model.Interfejs;
import rs.ac.singidunum.novisad.zadatak1.model.JavaGenerator;
import rs.ac.singidunum.novisad.zadatak1.model.Klasa;
import rs.ac.singidunum.novisad.zadatak1.model.Metoda;
import rs.ac.singidunum.novisad.zadatak1.model.Parametar;
import rs.ac.singidunum.novisad.zadatak1.model.PythonGenerator;

public class App {
	public static void main(String[] args) {
		Interfejs podatak = new Interfejs("Podatak", "public");
		Interfejs ispisiviPodatak = new Interfejs("IspisiviPodatak", "public");
		ispisiviPodatak.dodajNadtip(podatak);
		ispisiviPodatak.dodajMetodu(new Metoda("ispisi", "public", false, "void"));
		
		Klasa osoba = new Klasa("Osoba", "public", true);
		osoba.dodajNadtip(podatak);
		osoba.dodajAtribut(new Atribut("ime", "private", false, false, "String", null));
		osoba.dodajAtribut(new Atribut("prezime", "private", false, false, "String", null));
		osoba.dodajMetodu(new Metoda("getIme", "public", false, "String"));
		osoba.dodajMetodu(new Metoda("setIme", "public", false, "void", new Parametar("ime", "String")));
		osoba.dodajMetodu(new Metoda("getPrezime", "public", false, "String"));
		osoba.dodajMetodu(new Metoda("setPrezime", "public", false, "void", new Parametar("prezime", "String")));
		
		Klasa radnik = new Klasa("Radnik", "public", false);
		radnik.dodajNadtip(osoba);
		radnik.dodajNadtip(ispisiviPodatak);
		radnik.dodajAtribut(new Atribut("sifraZaposlenog", "public", false, false, "String", null));
		radnik.dodajAtribut(new Atribut("visinaPlate", "public", false, false, "double", "54000"));
		osoba.dodajMetodu(new Metoda("getSifraZaposlenog", "public", false, "String"));
		osoba.dodajMetodu(new Metoda("setSifraZaposlenog", "public", false, "void", new Parametar("sifraZaposleog", "String")));
		
		System.out.println("Java kod:");
		Generator javaGenerator = new JavaGenerator();
		System.out.println(javaGenerator.generisi(podatak));
		System.out.println(javaGenerator.generisi(ispisiviPodatak));
		System.out.println(javaGenerator.generisi(osoba));
		System.out.println(javaGenerator.generisi(radnik));
		
		System.out.println("Python kod:");
		Generator pythonGenerator = new PythonGenerator();
		System.out.println(pythonGenerator.generisi(podatak));
		System.out.println(pythonGenerator.generisi(ispisiviPodatak));
		System.out.println(pythonGenerator.generisi(osoba));
		System.out.println(pythonGenerator.generisi(radnik));
	}
}
