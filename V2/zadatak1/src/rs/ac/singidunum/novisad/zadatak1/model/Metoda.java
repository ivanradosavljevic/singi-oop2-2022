package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Metoda extends VidljiviElement {
	private boolean staticka;
	private String tipPovratneVrednosti;
	private List<Parametar> parametri = new ArrayList<>();

	public Metoda() {
		super();
	}

	public Metoda(String naziv, String vidljivost, boolean staticka, String tipPovratneVrednosti,
			List<Parametar> parametri) {
		super(naziv, vidljivost);
		this.staticka = staticka;
		this.tipPovratneVrednosti = tipPovratneVrednosti;
		this.parametri = parametri;
	}

	public Metoda(String naziv, String vidljivost, boolean staticka, String tipPovratneVrednosti,
			Parametar... parametri) {
		this(naziv, vidljivost, staticka, tipPovratneVrednosti, Arrays.asList(parametri));
	}

	public boolean isStaticka() {
		return staticka;
	}

	public void setStaticka(boolean staticka) {
		this.staticka = staticka;
	}

	public String getTipPovratneVrednosti() {
		return tipPovratneVrednosti;
	}

	public void setTipPovratneVrednosti(String tipPovratneVrednosti) {
		this.tipPovratneVrednosti = tipPovratneVrednosti;
	}

	public List<Parametar> getParametri() {
		return parametri;
	}

	public void setParametri(ArrayList<Parametar> parametri) {
		this.parametri = parametri;
	}
}
