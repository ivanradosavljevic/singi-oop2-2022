package rs.ac.singidunum.novisad.zadatak1.model;

public class Interfejs extends SlozeniTip {

	public Interfejs() {
		super();
	}

	public Interfejs(String naziv, String vidljivost) {
		super(naziv, vidljivost);
	}

	@Override
	public void dodajNadtip(SlozeniTip slozeniTip) {
		if (slozeniTip instanceof Interfejs && this != slozeniTip) {
			super.dodajNadtip(slozeniTip);
		} else {
			throw new IllegalArgumentException("Interfejs moze nasledjivati samo drugi interfejs!");
		}
	}
}
