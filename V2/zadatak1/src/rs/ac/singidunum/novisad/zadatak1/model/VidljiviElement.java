package rs.ac.singidunum.novisad.zadatak1.model;

public abstract class VidljiviElement extends ImenovaniElement {
	private String vidljivost;

	public VidljiviElement() {
	}

	public VidljiviElement(String naziv, String vidljivost) {
		super(naziv);
		this.vidljivost = vidljivost;
	}

	public String getVidljivost() {
		return vidljivost;
	}

	public void setVidljivost(String vidljivost) {
		this.vidljivost = vidljivost;
	}
}
