package rs.ac.singidunum.novisad.zadatak1.model;

import java.util.List;

public class PythonGenerator implements Generator {
	protected String generisiListuParametara(List<? extends ImenovaniElement> parametri) {
		String kod = "";
		if (parametri.size() > 0) {
			for (int i = 0; i < parametri.size() - 1; i++) {
				kod += this.generisi(parametri.get(i)) + ", ";
			}
			kod += this.generisi(parametri.get(parametri.size() - 1));
		}
		return kod;
	}

	protected String generisi(ImenovaniElement imenovaniElement) {
		if (imenovaniElement instanceof Parametar) {
			return this.generisi((Parametar) imenovaniElement);
		} else if (imenovaniElement instanceof Atribut) {
			return this.generisi((Atribut) imenovaniElement);
		} else if(imenovaniElement instanceof SlozeniTip) {
			return imenovaniElement.getNaziv();
		}
		return null;
	}
	
	protected String generisiSlozeniTip(SlozeniTip slozeniTip) {
		String kod = "class " + slozeniTip.getNaziv();
		if(slozeniTip.getNasledjuje().size() > 0) {
			kod += "(" + generisiListuParametara(slozeniTip.getNasledjuje()) + ")";
		}
		kod += ":\n";
		kod += "   def __init__(";
		kod += generisiListuParametara(slozeniTip.getAtributi());
		kod += "):\n      pass\n";
		for (Metoda m : slozeniTip.getMetode()) {
			kod += "   " + this.generisi(m) + "\n";
		}
		return kod;
	}

	@Override
	public String generisi(Parametar parametar) {
		return parametar.getNaziv();
	}

	@Override
	public String generisi(Atribut atribut) {
		return atribut.getNaziv() + (atribut.getPocetnaVrednost() == null ? "" : "=" + atribut.getPocetnaVrednost());
	}

	@Override
	public String generisi(Metoda metoda) {
		String kod = "def ";
		kod += metoda.getNaziv() + "(" + generisiListuParametara(metoda.getParametri()) + "):\n";
		kod += "      pass";
		return kod;
	}

	@Override
	public String generisi(Interfejs interfejs) {
		return this.generisiSlozeniTip(interfejs);
	}

	@Override
	public String generisi(Klasa klasa) {
		return this.generisiSlozeniTip(klasa);
	}
}
