# Zadatak 1

1. Napisati klase koje opisuju koncepte objektno orijentisanog programiranja, odnosno opisuju klase, interfejse, atribute i metode.
2. Napisati klasu Generator koja instance prethodno navdenih klasa transformiše u tekstualni format. Format treba da odgovara ispravnoj sintaksi programskog jezika Java.
3. Definisati generator za programski jezik Python.
4. Testirati rešenje instanciranjem nekoliko klasa i oba generatora. Generisati Python i Java kod za napravljene instance.