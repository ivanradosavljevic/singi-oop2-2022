#include <iostream>

#include "headers/oblik.h"
#include "headers/pravougaonik.h"
#include "headers/krug.h"

using namespace std;

int main() {
	Pravougaonik *p = new Pravougaonik(10.2, 18.5);
	Krug *k = new Krug(10);

	cout << p->povrsina() << endl;
	cout << k->povrsina() << endl;
	Oblik *o = new Pravougaonik(10.2, 18.5);

	cout << o->povrsina() << endl;

	return 0;
}
