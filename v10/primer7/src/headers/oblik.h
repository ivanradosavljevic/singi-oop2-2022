#ifndef HEADERS_OBLIK_H_
#define HEADERS_OBLIK_H_

class Oblik{
public:
	Oblik();
	virtual double povrsina()=0;
	virtual ~Oblik(){};
};

#endif /* HEADERS_OBLIK_H_ */
