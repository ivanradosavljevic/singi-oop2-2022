#include <iostream>

#include "headers/pravougaonik.h"
#include "headers/krug.h"

using namespace std;

int main() {
	Pravougaonik p(10.2, 18.5);
	Krug k(10);

	cout << p.povrsina() << endl;
	cout << k.povrsina() << endl;

	Oblik o = Pravougaonik(10.2, 18.5);

	cout << o.povrsina() << endl;

	return 0;
}
