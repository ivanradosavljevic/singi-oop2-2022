#include <iostream>
#include <vector>


#include "headers/oblik.h"
#include "headers/pravougaonik.h"
#include "headers/krug.h"

using namespace std;

int main() {
	vector<Oblik*> vektor;
	vektor.push_back(new Pravougaonik(10.2, 18.5));
	vektor.push_back(new Krug(10));

	for(unsigned int i = 0; i < vektor.size(); i++) {
		cout << vektor[i]->povrsina() << endl;
	}

	return 0;
}
