#include <iostream>

#include "headers/oblik.h"
#include "headers/pravougaonik.h"
#include "headers/krug.h"

using namespace std;

int main() {
	Oblik *p = new Pravougaonik(10.2, 18.5);
	Oblik *k = new Krug(10);
	cout << p->povrsina() << endl;
	cout << k->povrsina() << endl;
	return 0;
}
