# Zadatak 3

1. Napraviti klasu koja predstavlja kolekciju koja u sebi može sadržati samo cele brojeve. Kolekcija treba da podržava operacije za dodavanje i uklanjanje brojeva u kolekciju.
2. Napraviti klasu Proizvođač koja u nasumičnim trenucima dodaje nasumične vrednosti u kolekciju.
3. Napraviti klasu Potrošač koja iz kolekcije vadi dostupne vrednosti sve dok ih ima.
4. Potrošače i proizvođače modelovati kao niti.
5. Omogućiti da kolekcija funkcioniše sa više potrošača i proizvođača.