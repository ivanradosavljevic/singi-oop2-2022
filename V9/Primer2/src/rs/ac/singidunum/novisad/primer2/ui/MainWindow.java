package rs.ac.singidunum.novisad.primer2.ui;

import java.awt.BorderLayout;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 3211406970430814253L;

	public MainWindow() throws HeadlessException {
		super();
	}

	public MainWindow(GraphicsConfiguration gc) {
		super(gc);
	}

	public MainWindow(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

	public MainWindow(String title) throws HeadlessException {
		super(title);
	}

	public void init() {
		this.setTitle("Glavni prozor");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenuItem = new JMenuItem("Exit");

		JToolBar toolbar = new JToolBar("Toolbar");
		JButton button = new JButton("Exit");
		button.addActionListener(e -> System.exit(0));

		toolbar.add(button);

		exitMenuItem.addActionListener(e -> {
			System.exit(0);
		});

		fileMenu.add(exitMenuItem);
		menuBar.add(fileMenu);

		RadnikForm radnikForma = new RadnikForm();
		
		radnikForma.addActionListener(e->{
			System.out.println(radnikForma.getFormData());
		});

		this.setJMenuBar(menuBar);

		this.getContentPane().add(toolbar, BorderLayout.PAGE_START);
		this.getContentPane().add(radnikForma, BorderLayout.CENTER);
		this.setVisible(true);
	}
}
