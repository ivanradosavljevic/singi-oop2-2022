package rs.ac.singidunum.novisad.primer2;

import rs.ac.singidunum.novisad.primer2.ui.MainWindow;

public class App {

	public static void main(String[] args) {
		MainWindow mainWindow = new MainWindow();
		mainWindow.init();
	}

}
