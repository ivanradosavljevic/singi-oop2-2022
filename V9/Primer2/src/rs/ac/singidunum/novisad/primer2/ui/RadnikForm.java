package rs.ac.singidunum.novisad.primer2.ui;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import rs.ac.singidunum.novisad.primer2.model.Radnik;

public class RadnikForm extends JPanel {
	private static final long serialVersionUID = 4603628585687231846L;
	protected JTextField nameInput = new JTextField(20);
	protected JTextField surnameInput = new JTextField(20);
	protected SpringLayout springLayout = new SpringLayout();
	protected JButton button = new JButton("Dodaj");
	
	public RadnikForm() {
		super();
		this.setLayout(this.springLayout);
		this.init();
	}
	
	private void init() {
		JLabel nameLabel = new JLabel("Ime: ");
		JLabel surnameLabel = new JLabel("Prezime: ");
		
		this.add(nameLabel);
		this.add(nameInput);
		this.add(surnameLabel);
		this.add(surnameInput);
		this.add(button);
		
		this.springLayout.putConstraint(SpringLayout.EAST, nameLabel, 100, SpringLayout.WEST, this);
		
		this.springLayout.putConstraint(SpringLayout.NORTH, nameInput, 10, SpringLayout.NORTH, this);
		this.springLayout.putConstraint(SpringLayout.WEST, nameInput, 10, SpringLayout.EAST, nameLabel);
		this.springLayout.putConstraint(SpringLayout.EAST, nameInput, -10, SpringLayout.EAST, this);
		
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, nameLabel, 0, SpringLayout.VERTICAL_CENTER, nameInput);
		
		this.springLayout.putConstraint(SpringLayout.EAST, surnameLabel, 100, SpringLayout.WEST, this);
		
		this.springLayout.putConstraint(SpringLayout.NORTH, surnameInput, 10, SpringLayout.SOUTH, nameInput);
		this.springLayout.putConstraint(SpringLayout.WEST, surnameInput, 10, SpringLayout.EAST, surnameLabel);
		this.springLayout.putConstraint(SpringLayout.EAST, surnameInput, -10, SpringLayout.EAST, this);
		
		this.springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, surnameLabel, 0, SpringLayout.VERTICAL_CENTER, surnameInput);
		
		this.springLayout.putConstraint(SpringLayout.NORTH, button, 10, SpringLayout.SOUTH, surnameInput);
		this.springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, button, 0, SpringLayout.HORIZONTAL_CENTER, this);
	}
	
	public Radnik getFormData() {
		return new Radnik(nameInput.getText(), surnameInput.getText());
	}
	
	public void addActionListener(ActionListener listener) {
		this.button.addActionListener(listener);
	}
}
