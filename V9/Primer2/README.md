# Primer 2
1. Napisati model za radnika. Radnik je opisan imenom i prezimenom.
2. Napraviti klasu MainWindow koja predstavlja specifikaciju glavnog prozora.
3. Uvesti klasu RadnikForma koja u sebi definiše formu za kreiranje nove instance radnika.
4. Dodati dugme kojim se može ispisati sadržaj popunjen u formi. Pre ispisa sadržaj forme mora biti pretvoren u objekat tipa Radnik.