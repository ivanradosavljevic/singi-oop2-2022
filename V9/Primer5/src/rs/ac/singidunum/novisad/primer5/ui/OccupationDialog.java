package rs.ac.singidunum.novisad.primer5.ui;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import rs.ac.singidunum.novisad.primer5.model.Zanimanje;

public class OccupationDialog extends JDialog {
	private static final long serialVersionUID = 1551167255198553963L;
	private SpringLayout springLayout = new SpringLayout();
	private JTextField occupationName = new JTextField();
	private JButton confirm = new JButton("Dodaj");
	private JButton cancel = new JButton("Odustani");
	private Zanimanje zanimanje = null;

	public OccupationDialog() {
		super();
		this.init();
	}

	protected void init() {
		this.setModal(true);
		this.setTitle("Zanimanje dijalog");
		this.setLayout(springLayout);
		this.add(this.occupationName);
		this.add(confirm);
		this.add(cancel);

		this.setSize(200, 200);

		this.springLayout.putConstraint(SpringLayout.NORTH, this.occupationName, 10, SpringLayout.NORTH,
				this.getContentPane());
		this.springLayout.putConstraint(SpringLayout.WEST, this.occupationName, 10, SpringLayout.WEST,
				this.getContentPane());
		this.springLayout.putConstraint(SpringLayout.EAST, this.occupationName, -10, SpringLayout.EAST,
				this.getContentPane());

		this.springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, this.confirm, 0, SpringLayout.HORIZONTAL_CENTER,
				this.getContentPane());
		this.springLayout.putConstraint(SpringLayout.NORTH, this.confirm, 10, SpringLayout.SOUTH, this.occupationName);

		this.springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, this.cancel, 0, SpringLayout.HORIZONTAL_CENTER,
				this.getContentPane());
		this.springLayout.putConstraint(SpringLayout.NORTH, this.cancel, 10, SpringLayout.SOUTH, this.confirm);

		this.confirm.addActionListener(e -> {
			this.zanimanje = new Zanimanje(this.occupationName.getText());
			this.dispose();
		});

		this.cancel.addActionListener(e -> {
			this.dispose();
		});
	}

	public Zanimanje getZanimanje() {
		return zanimanje;
	}
}
