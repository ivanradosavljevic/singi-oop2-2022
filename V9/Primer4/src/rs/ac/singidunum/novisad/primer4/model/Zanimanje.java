package rs.ac.singidunum.novisad.primer4.model;

public class Zanimanje {
	private String naziv;

	public Zanimanje(String naziv) {
		super();
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public String toString() {
		return "Zanimanje [naziv=" + naziv + "]";
	}
}
