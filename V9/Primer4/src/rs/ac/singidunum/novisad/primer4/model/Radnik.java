package rs.ac.singidunum.novisad.primer4.model;

public class Radnik {
	private String ime;
	private String prezime;
	private double visinaPlate;
	private Zanimanje zanimanje;

	public Radnik(String ime, String prezime, double visinaPlate, Zanimanje zanimanje) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.visinaPlate = visinaPlate;
		this.zanimanje = zanimanje;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public double getVisinaPlate() {
		return visinaPlate;
	}

	public void setVisinaPlate(double visinaPlate) {
		this.visinaPlate = visinaPlate;
	}

	public Zanimanje getZanimanje() {
		return zanimanje;
	}

	public void setZanimanje(Zanimanje zanimanje) {
		this.zanimanje = zanimanje;
	}

	@Override
	public String toString() {
		return "Radnik [ime=" + ime + ", prezime=" + prezime + ", visinaPlate=" + visinaPlate + ", zanimanje="
				+ zanimanje + "]";
	}
}
